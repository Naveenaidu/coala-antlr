"""
NodeData module.

Contains the NodeData class that helps transfer information from walker to
bear.
"""


class NodeData(object):
    """
    Helper class to represent information from parse-tree.

    A class that helps convey accessory information like
    column no and class number for a node easily.
    """

    def __init__(self, txt, lineNo=None, colNo=None, other=None):
        """
        Construct NodeData.

        :param txt:     The text that this node is supposed to have in
                        original source.
        :param lineNo:  Line number of this node's text's first character
                        in original source.
        :param colNo:   Column number of this node's text's first character
                        in original source.
        :param other:   Any auxilary information that would be of use.
        """
        self.text = txt
        self.lineNo = lineNo
        self.colNo = colNo
        self.other = other

    def __str__(self):
        """Return the text that this Node represents."""
        return self.text
