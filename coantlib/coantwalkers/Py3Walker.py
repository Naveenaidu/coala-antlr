"""Py3Walker module."""

from coantlib.ASTWalker import ASTWalker
from coantlib.coantparsers.Python3 import Python3Parser
from coantlib.coantparsers.Python3 import Python3Visitor
from coantlib.commonutils import flatten, get_line_and_col
from coantlib.NodeData import NodeData

import types


class Py3Walker(ASTWalker):
    """Walker class for Python3's ANTLR Parse Tree."""

    def __init__(self, filename, file, lang):
        """
        Initialise the parent classes for creating a Parser.

        :param filename:    Name of file on which this walker is supposed to
                            crawl.
        :param file:        Contents of the file on which this walker is
                            supposed to crawl.
        :param lang:        Language of the file.
        """
        super(Py3Walker, self).__init__(file, filename, lang)

    def get_quote_content(self):
        r"""
        Crawl contents inside strings in the source code.

        This function returns a list of NodeData containing
        information about strings in a python source code::

            [
                NodeData('a string in source'),
                NodeData('another string in source'),
            ]

        Say the source is::

            a_python_variable = 'String using ' "implicit concatenation"

        Then a list containing a single NodeData will be returned::

            NodeData(
                "\'String using \'\"implicit concatenation\"",
                1,
                21
            )

        """
        visitor = Python3Visitor()
        visitor.results = []

        def strrLambda(self, ctx):
            lin, col = get_line_and_col(ctx)
            self.results.append(NodeData(ctx.parentCtx.getText(),
                                         lin,
                                         col))
            return self.visitChildren(ctx)
        visitor.visitStrr = types.MethodType(strrLambda, visitor)
        visitor.visit(self.treeRoot)

        return visitor.results

    def get_functions(self):
        """
        Crawl over functions and fetch their signatures.

        :return: NodeData Objects representing functions in the source code
        :rtype: Dictionary

        :Example:

        The return value is a dictionary of the form::

            {
                'NodeData(function_name)': {
                        'args': [NodeData(),
                                 NodeData(),
                                 NodeData(),
                                 NodeData()
                                ],
                        '*': NodeData(),
                        '**': NodeData()
                 }
            }

        Each NodeData object contains information about the line and column
        number along with the variable used for this argument
        """
        visitor = Python3Visitor()
        visitor.functions = {}

        def functionLambda(self, ctx):
            func_name = ctx.parentCtx.getChild(1).getText()
            func_line = ctx.parentCtx.getChild(1).getPayload().line
            func_col = ctx.parentCtx.getChild(1).getPayload().column+1
            fname = NodeData(func_name, func_line, func_col)
            self.functions[fname] = {
                'args': [],
                '*': None,
                '**': None,
            }
            argsNode = None
            for child in ctx.getChildren():
                if isinstance(child, Python3Parser.TypedargslistContext):
                    argsNode = child
            if not argsNode:
                return

            for child in argsNode.getChildren():
                if isinstance(child, Python3Parser.TfpdefContext):
                    var = child.getChild(0).getText()
                    linNum = child.getChild(0).getPayload().line
                    colNum = child.getChild(0).getPayload().column+1
                    if ('*' in self.functions[fname] and
                            self.functions[fname]['*'] == -1):
                        self.functions[fname]['*'] = NodeData(
                                                        var,
                                                        linNum,
                                                        colNum
                                                     )
                    elif ('**' in self.functions[fname] and
                            self.functions[fname]['**'] == -1):
                        self.functions[fname]['**'] = NodeData(
                                                        var,
                                                        linNum,
                                                        colNum
                                                      )
                    else:
                        self.functions[fname]['args'].append(
                            NodeData(var, linNum, colNum))
                elif child.getText() == '*':
                    self.functions[fname]['*'] = -1
                elif child.getText() == '**':
                    self.functions[fname]['**'] = -1

        visitor.visitParameters = types.MethodType(functionLambda, visitor)
        visitor.visit(self.treeRoot)
        return visitor.functions

    def get_assignments(self):
        """
        Crawl over all variables that have been assigned a value.

        This function will fetch all the variable assignments and return
        them along with their assigned values.

        Does not preserve the type of sequence, all of them are kept as tuples
        i.e an assignment statement like:
        [x,y] = [1,2]

        will be transformed and returned as:
        [(NodeData(x), NodeData(1)) , (NodeData(y), NodeDate(2))]

        and
        x = [1,2]
        will be transformed as:
        [(NodeData(x), (NodeData(1), NodeData(2)))]

        Although the ``NodeData.other`` field will often contain information
        about the type of sequence, i.e ``()``, ``[]``, ``{}``.
        """
        class Visitor(Python3Visitor):

            def __init__(self):
                self.type_seq = None
                self.visitTest = self.genericFunc
                self.visitLambdef = self.genericFunc
                self.visitLambdef_nocond = self.genericFunc
                self.visitTest_nocond = self.genericFunc
                self.visitOr_test = self.genericFunc
                self.visitAnd_test = self.genericFunc
                self.visitNot_test = self.genericFunc
                self.visitComparision = self.genericFunc
                self.visitExpr = self.genericFunc
                self.visitXor_expr = self.genericFunc
                self.visitAnd_expr = self.genericFunc
                self.visitShift_expr = self.genericFunc
                self.visitArith_expr = self.genericFunc
                self.visitTerm = self.genericFunc
                self.visitFactor = self.genericFunc
                self.visitPower = self.genericFunc

            def visitExpr_stmt(self, ctx):
                equalsNode = []
                for x in range(ctx.getChildCount()):
                    cur_child = ctx.getChild(x)
                    if cur_child.getText() == '=':
                        equalsNode.append(x)
                if len(equalsNode) > 0:
                    # we can get some assignment statement from this node
                    index = 0
                    for node in ctx.getChildren():
                        if (index != ctx.getChildCount()-1 and
                            isinstance(node,
                                       Python3Parser.Testlist_star_exprContext
                                       )):
                            resNode = ctx.getChild(ctx.getChildCount()-1)
                            lvar = self.visitChildren(node)
                            rvar = self.visitChildren(resNode)
                            # lvar will always have a value, since
                            # an = node cannot have None on the lhs
                            if (isinstance(lvar[0], tuple) and
                                    rvar and
                                    isinstance(rvar[0], NodeData)):
                                # lhs is a tuple but rhs could not be
                                # placed as a suitable type
                                lvar = list(lvar[0])

                            self.lhs += lvar
                            if len(rvar) == 1 and len(lvar) != 1:
                                new_rvar = []
                                for x in lvar:
                                    new_rvar.append(rvar[0])
                                rvar = new_rvar

                            self.rhs += rvar
                        index += 1
                else:
                    return self.visitChildren(ctx)

            def genericFunc(self, ctx):
                if ctx.getChildCount() > 1:
                    (line, col) = get_line_and_col(ctx)
                    return [NodeData(ctx.getText(), line, col, self.type_seq)]
                return self.visitChildren(ctx)

            def visitTerminal(self, node):
                if node.getText() in [',', '[', ']', '{', '}', '(', ')']:
                    return self.defaultResult()
                return [
                        NodeData(node.getText(),
                                 node.getPayload().line,
                                 node.getPayload().column+1,
                                 self.type_seq
                                 )
                        ]

            def visitAtom(self, ctx):
                if (ctx.getText() in ['[]', '()', '{}'] or
                    (ctx.getChild(0).getText() not in ['[', '.', '(', '{']) and
                        ctx.getChildCount() > 1):
                    (lin, col) = get_line_and_col(ctx)
                    return [(NodeData(ctx.getText(), lin, col, self.type_seq),)]
                return self.visitChildren(ctx)

            def visitInteger(self, ctx):
                return [
                        NodeData(ctx.getText(),
                                 ctx.getChild(0).getPayload().line,
                                 ctx.getChild(0).getPayload().column+1,
                                 self.type_seq
                                 )
                        ]

            def visitStrr(self, ctx):
                return [
                        NodeData(ctx.getText()[1:-1],
                                 ctx.getChild(0).getPayload().line,
                                 ctx.getChild(0).getPayload().column+1,
                                 self.type_seq
                                 )
                        ]

            def visitDictorsetmaker(self, ctx):
                temp = self.type_seq
                self.type_seq = '{}'
                val = [tuple(self.visitChildren(ctx))]
                self.type_seq = temp
                return val

            def visitTestlist_comp(self, ctx):
                temp = self.type_seq
                # retain the outermost type for the nodes
                if ctx.parentCtx.getChild(0).getText() == '[' and temp is None:
                    self.type_seq = '[]'
                elif (ctx.parentCtx.getChild(0).getText() == '('
                      and temp is None):
                    self.type_seq = '()'
                res = self.visitChildren(ctx)
                self.type_seq = temp
                if ctx.getChildCount() > 1 and res:
                    return [tuple(res)]
                return res

            def aggregateResult(self, aggregate, nextResult):
                if not nextResult:
                    return aggregate
                elif not aggregate:
                    return nextResult
                else:
                    return aggregate + nextResult

        visitor = Visitor()
        visitor.lhs = []
        visitor.rhs = []

        visitor.visit(self.treeRoot)
        t = flatten(visitor.lhs, visitor.rhs)
        return t
