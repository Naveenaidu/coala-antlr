"""Bear to detect trailing spaces in python strings."""

from coantlib.ASTBear import ASTBear
from coalib.results.Result import Result


class PyQuoteSpacingBear(ASTBear):
    r"""
    Check for trailing spaces in source strings.

    Example:

    .. code:: Python

        x = 'A string  '

    This bear will detect a trailing space in ``x`` above.

    It also works with line continuation ``\\`` and implicit string
    concatenation in Python.
    """

    LANGUAGES = {'Python 3'}
    CAN_DETECT = {'Formatting'}

    def run(self,
            filename,
            file,
            min_str_len=3):
        """
        Check for trailing spaces.

        :param min_str_len:     The minimum length of a string to check for
                                trailing spaces.
        """
        super().run(filename, file)
        quoteContent = self.walker.get_quote_content()
        for content in quoteContent:
            if str(content)[-2] == ' ' and len(str(content)) > min_str_len:
                yield Result.from_values(
                    origin=self,
                    message=('Line {} has a string with trailing space '
                             '{}').format(content.lineNo, content.colNo),
                    file=filename,
                    line=content.lineNo,
                    column=content.colNo,
                    end_line=content.lineNo,
                    end_column=content.colNo+len(content.text))
